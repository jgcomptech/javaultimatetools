package com.jgcomptech.tools.databasetools.jbdc;

/**
 * Connection info object to be used to connect to a database
 */
public class DatabaseConnectionInfo {
    public String Path = "";
    public String Username = "";
    public String Password = "";
    public DatabaseType DBType;
}

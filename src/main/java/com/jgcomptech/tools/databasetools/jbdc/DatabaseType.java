package com.jgcomptech.tools.databasetools.jbdc;

/**
 * Database type to be used to determine database operations
 */
public enum DatabaseType {
    H2,
    SQLite
}
